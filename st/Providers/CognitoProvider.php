<?php

namespace ST\Providers;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use pmill\AwsCognito\CognitoClient;
use pmill\LaravelAwsCognito\Guards\CognitoGuard;
use ST\AwsCognito\CognitoClient as StCognitoClient;

class CognitoProvider extends ServiceProvider {
  /**
   * Register cognito services.
   *
   * @return void
   */
  public function register() {
    // dump('StCognitoProvider.register');
  }

  /**
   * Boot cognito services.
   *
   * @return void
   */
  public function boot() {
    $this->publishes([
      __DIR__ . '/../../config/aws-cognito-auth.php' => config_path('aws-cognito-auth.php'),
    ], 'config');
    $this->mergeConfigFrom(
      __DIR__ . '/../../config/aws-cognito-auth.php', 'aws-cognito-auth'
    );

    $this->app->singleton('aws-cognito-sdk', function (Application $app) {
      return new \Aws\Sdk(config('aws-cognito-auth'));
    });

    $this->app->singleton(StCognitoClient::class, function (Application $app) {
      $awsCognitoIdentityProvider = $app->make('aws-cognito-sdk')->createCognitoIdentityProvider();
      $cognitoClient = new StCognitoClient($awsCognitoIdentityProvider);
      $cognitoClient->setAppClientId(config('aws-cognito-auth.app_client_id'));
      $cognitoClient->setAppClientSecret(config('aws-cognito-auth.app_client_secret'));
      $cognitoClient->setRegion(config('aws-cognito-auth.region'));
      $cognitoClient->setUserPoolId(config('aws-cognito-auth.user_pool_id'));
      return $cognitoClient;
    });

    // rename CognitoClient to StCognitoClient
    $this->app->singleton(CognitoClient::class, function (Application $app) {
      return $app->make(StCognitoClient::class);
    });

    $this->app['auth']->extend('aws-cognito', function (Application $app, $name, array $config) {
      $client = $app->make(CognitoClient::class);
      $provider = $app['auth']->createUserProvider($config['provider']);
      return new CognitoGuard($provider, $client);
    });
  }

}
