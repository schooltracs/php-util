<?php

namespace ST\AwsCognito;

use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Hidehalo\Nanoid\Client as NanoidClient;
use pmill\AwsCognito\CognitoClient as BaseCognitoClient;
use pmill\AwsCognito\Exception\ChallengeException;
use pmill\AwsCognito\Exception\CognitoResponseException;
use pmill\AwsCognito\Exception\TokenExpiryException;
use pmill\AwsCognito\Exception\TokenVerificationException;

class CognitoClient extends BaseCognitoClient {
  private $usernameLength = 21;

  public function __construct(CognitoIdentityProviderClient $client) {
    parent::__construct($client);
  }

  /**
   * Verifies the given access token and returns the username
   *
   * @param string $accessToken
   *
   * @throws TokenExpiryException
   * @throws TokenVerificationException
   *
   * @return string
   */
  public function verifyIdToken($accessToken) {
    $jwtPayload = $this->decodeAccessToken($accessToken);

    $expectedIss = sprintf('https://cognito-idp.%s.amazonaws.com/%s', $this->region, $this->userPoolId);
    if ($jwtPayload['iss'] !== $expectedIss) {
      throw new TokenVerificationException('invalid iss');
    }

    if ($jwtPayload['token_use'] !== 'id') {
      throw new TokenVerificationException('invalid token_use');
    }

    if ($jwtPayload['exp'] < time()) {
      throw new TokenExpiryException('invalid exp');
    }

    return $jwtPayload;
  }

  public function adminUserGlobalSignOut($username) {
    $response = $this->client->adminUserGlobalSignOut([
      'Username' => $username,
      'UserPoolId' => $this->userPoolId,
    ]);
    $statusCode = data_get($response->get('@metadata'), 'statusCode');
    return ['statusCode' => $statusCode];
  }

  public function createUser($username, $password, array $attributes = []) {
    $userAttributes = $this->buildAttributesArray($attributes);

    try {
      $response = $this->client->adminCreateUser([
        'UserPoolId' => $this->userPoolId,
        'TemporaryPassword' => $password,
        'UserAttributes' => $userAttributes,
        'Username' => $username,
        'MessageAction' => 'SUPPRESS',
      ]);

      // confirm the new user
      try {
        $this->authenticate($username, $password);
      } catch (ChallengeException $e) {
        if ($e->getChallengeName() === static::CHALLENGE_NEW_PASSWORD_REQUIRED) {
          $this->respondToNewPasswordRequiredChallenge($username, $password, $e->getSession());
        }
      }

      return $this->getUser($username);
    } catch (CognitoIdentityProviderException $e) {
      throw CognitoResponseException::createFromCognitoException($e);
    }
  }

  /**
   * Call the ListUsers api.
   *
   * https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-cognito-idp-2016-04-18.html#listusers
   */
  public function listUsers(array $filter = [], $limit = 50, $attributesToGet = null, $paginationToken = null) {
    if (!is_null($filter) && count($filter) > 3) {
      throw new \InvalidArgumentException('The first argument must be of array type with size less than or equal to 3. It was ' . $filter);
    }

    try {
      $params = [
        'Filter' => $this->parseFilterArray($filter),
        'Limit' => $limit,
        'UserPoolId' => $this->userPoolId,
      ];
      if ($attributesToGet) {
        $params['AttributesToGet'] = $attributesToGet;
      }
      if ($paginationToken) {
        $params['PaginationToken'] = $paginationToken;
      }
      $response = $this->client->listUsers($params);
      return $response;
    } catch (CognitoIdentityProviderException $e) {
      throw CognitoResponseException::createFromCognitoException($e);
    }
  }

  public function setPassword($username, $password) {
    try {
      $response = $this->client->adminSetUserPassword([
        'Password' => $password,
        'Permanent' => true,
        'UserPoolId' => $this->userPoolId,
        'Username' => $username,
      ]);
      return $response;
    } catch (CognitoIdentityProviderException $e) {
      throw CognitoResponseException::createFromCognitoException($e);
    }
  }

  public function deleteUserByUsername($username) {
    try {
      $this->client->adminDeleteUser([
        'UserPoolId' => $this->userPoolId,
        'Username' => $username,
      ]);
    } catch (CognitoIdentityProviderException $e) {
      throw CognitoResponseException::createFromCognitoException($e);
    }
  }

  public function generateUsername() {
    return (new NanoidClient())->generateId($this->usernameLength);
  }

  public function toPreferredUsername($email) {
    $email = trim($email);
    return strpos($email, '%') === false ? rawurlencode($email) : $email;
  }

  /**
   * Copy from pmill\AwsCognito\CognitoClient since it is private function
   * @param array $attributes
   * @return array
   */
  private function buildAttributesArray(array $attributes): array
  {
    $userAttributes = [];
    foreach ($attributes as $key => $value) {
      $userAttributes[] = [
        'Name' => (string) $key,
        'Value' => (string) $value,
      ];
    }
    return $userAttributes;
  }

  private function parseFilterArray($filterArray) {
    $name = isset($filterArray['name']) ? $filterArray['name'] : null;
    $type = isset($filterArray['type']) && $filterArray['type'] == 'startsWith' ? '^=' : '=';
    $value = isset($filterArray['value']) ? str_replace('"', '\"', $filterArray['value']) : null;
    return empty($name) ? null : $name . $type . '"' . $value . '"';
  }
}