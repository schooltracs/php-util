<?php

namespace ST\Services;

use ST\AwsCognito\CognitoClient;

class Cognito extends CognitoClient {
  public function __construct(CognitoClient $client) {
    $this->client = $client;
  }

  public function verifyIdToken($accessToken) {
    $r = $this->client->verifyIdToken($accessToken);
    return new IdTokenUser($r);
  }

  public function verifyAccessToken($accessToken) {
    return $this->client->verifyAccessToken($accessToken);
  }

  public function decodeAccessToken($accessToken) {
    return $this->client->decodeAccessToken($accessToken);
  }

  public function refreshTokens($username, $refreshToken) {
    return $this->client->refreshAuthentication($username, $refreshToken);
  }

  public function adminUserGlobalSignOut($username) {
    return $this->client->adminUserGlobalSignOut($username);
  }

  public function toPreferredUsername($email) {
    return $this->client->toPreferredUsername($email);
  }

  public function toCamelKeys($arr) {
    return array_reduce(array_keys($arr), function ($carry, $key) use ($arr) {
      $carry[\Str::camel($key)] = $arr[$key];
      return $carry;
    }, []);
  }
}