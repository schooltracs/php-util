<?php

namespace ST\Services;

use Illuminate\Contracts\Auth\Authenticatable;

class IdTokenUser implements Authenticatable, \JsonSerializable {
  public $username;
  public $name;
  public $preferredUsername;

  public function __construct($r) {
    $this->username = data_get($r, 'username', data_get($r, 'cognito:username'));
    $this->name = data_get($r, 'name', data_get($r, 'name'));
    $this->preferredUsername = data_get($r, 'preferredUsername', data_get($r, 'preferred_username'));
  }

  /**
   * Get the name of the unique identifier for the user.
   *
   * @return string
   */
  public function getAuthIdentifierName() {
    return 'username';
  }

  /**
   * Get the unique identifier for the user.
   *
   * @return mixed
   */
  public function getAuthIdentifier() {
    return $this->username;
  }

  /**
   * Get the password for the user.
   *
   * @return string
   */
  public function getAuthPassword() {
  }

  /**
   * Get the token value for the "remember me" session.
   *
   * @return string
   */
  public function getRememberToken() {
  }

  /**
   * Set the token value for the "remember me" session.
   *
   * @param  string  $value
   * @return void
   */
  public function setRememberToken($value) {
  }

  /**
   * Get the column name for the "remember me" token.
   *
   * @return string
   */
  public function getRememberTokenName() {
  }

  public function jsonSerialize() {
    return get_object_vars($this);
  }
  public function __toString() {
    return $this->jsonSerialize();
  }
}
