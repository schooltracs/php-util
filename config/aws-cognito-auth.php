<?php

return [
  'credentials' => [
    'key' => env('AWS_COGNITO_KEY'),
    'secret' => env('AWS_COGNITO_SECRET'),
  ],
  'region' => env('AWS_COGNITO_REGION'),
  'version' => env('AWS_COGNITO_VERSION', 'latest'),

  'app_client_id' => env('AWS_COGNITO_CLIENT_ID'),
  'app_client_secret' => env('AWS_COGNITO_CLIENT_SECRET'),
  'user_pool_id' => env('AWS_COGNITO_USER_POOL_ID'),
  'username_field' => 'username',
];
